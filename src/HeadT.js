import React from 'react';

export default class HeadT extends React.Component{
    constructor(props){
    super(props)
    //this.mas=[];
    this.state={
        mas:[],
        sortel:[]
    }
    }
    componentWillReceiveProps(){
        var mas1=[];
        for (let desc in this.props.inf[0]){
            mas1.push(desc);
        }
        this.setState({mas:mas1})
        this.setState({sortel:this.props.sortel})
    }
    render(){
        console.log(this.state.sortel)
        this.HeadList=this.state.mas.map((e)=><td key={e} id={e}>{e.toUpperCase()}{(()=>{if(this.state.sortel[e] !== undefined){if(this.state.sortel[e] === true) return <strong>↑</strong>; else return <strong>↓</strong>} })()}</td>)
        return (
        <tr> 
            {this.HeadList}
        </tr>
            )
    }
}