import React from 'react';
import HeadT from './HeadT.js';
import MainT from './MainT.js';
import  './index.css';


export default class App extends React.Component{
	constructor(props){
        super(props)
        this.state={
            index : 0,
            max_index: 0,
            res : [],
            sort : []
        }

           
       
    }
    componentDidMount(){
        let res1 = [ 
            {
                "Name": "Cairo Ramirez",
                "Email": "ultrices.Duis@indolorFusce.co.uk",
                "Phone": "0937 399 0249",
                "Company": "Mauris A LLP"
            },
            {
                "Name": "Nyssa Singleton",
                "Email": "pharetra.Nam.ac@euturpisNulla.ca",
                "Phone": "070 5584 6949",
                "Company": "Eu Tellus Limited"
            },
            {
                "Name": "Blossom Nielsen",
                "Email": "nunc.ac.mattis@senectuset.com",
                "Phone": "0979 983 9589",
                "Company": "At Company"
            },
            {
                "Name": "Ivan Merrill",
                "Email": "natoque.penatibus@nibhsitamet.net",
                "Phone": "0800 1111",
                "Company": "Tempus Scelerisque Consulting"
            }
        ]
    fetch('/api/excursions').then((e)=>{return e.json()}).then((resp)=>{this.setState({res:resp});console.log(resp);console.log(resp)})
        .catch((e)=>{this.setState({res:res1});console.log(e)})
        .catch(()=>{ this.mas=[];
        for (let desc in this.state.res[0]){
            this.mas.push(desc);
        }
        this.obj = {};
        for(let i=0;i<this.mas.length;i++) {this.obj[this.mas[i]]=undefined; };
        this.setState({sort:[this.obj]})
    })
    .then(()=>{ this.mas=[];
        for (let desc in this.state.res[0]){
            this.mas.push(desc);
        }
        this.obj = {};
        for(let i=0;i<this.mas.length;i++) {this.obj[this.mas[i]]=undefined;console.log(this.obj)};
        this.setState({sort:this.obj})
    })
}
    next(){
        console.log(this.state.length,this.state.index+15)
        if (this.state.index<this.state.res.length-15)
        this.setState({index: this.state.index+15})
    }
    back(){
        if (this.state.index>0)
        this.setState({ index: this.state.index - 15})
    }
    updateData = (value) => {
        this.setState({ max_index: value })
      }
      click=(e)=>{
          for(let i in this.state.sort) {
            console.log(i);
              if (e.target.id === i) { this.obj[i] = !this.obj[i]; console.log(this.obj[i]); this.setState({sort:this.obj})}
              else {this.obj[i] = undefined;  this.setState({sort:this.obj})} 
          }
          
      }
    render(){
        return (
            <div>
            <table onClick={this.click}>
                {(this.state.res !== []) && <HeadT inf={this.state.res} sortel={this.state.sort}/>}
                {(this.state.res !== []) && <MainT inf={this.state.res} k={this.state.index} updateData={this.updateData} sortel={this.state.sort}/>}
            </table>
            <div className="flex1">
                <button onClick={this.back.bind(this)}>{'назад'}</button>
                <div className="number">{`${this.state.index/15 + 1}`}</div>
                <button onClick={this.next.bind(this)}>{'вперед'}</button>
            </div>
            </div>
        )
    }
}
