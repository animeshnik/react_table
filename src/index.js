import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'

const st={
    color:'red'
}
ReactDOM.render(
    <Router>
        <Switch>
        <Route exact path="/" component={()=>(<div><Link style={st} to="/excursions">excursions</Link><br/><Link style={st} to="/about">about</Link></div>)}/>
        <Route exact path="/excursions" component={App}/>
        <Route exact path="/about" component={()=>(<div><p>about page</p><Link style={st} to="/">main</Link></div>)}/>
        </Switch>
    </Router>
    , document.getElementById('root')
);

